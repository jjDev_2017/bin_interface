import 'dart:io';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

//REFERENCE: https://github.com/AndreaMiotto/FlutterDialogs

enum ActionStyle { normal, destructive, important, important_destructive }

class CustomPopup extends StatefulWidget{
  @override CustomPopupState createState() => CustomPopupState();
}

class CustomPopupState extends State<CustomPopup>{
  // Create a text controller and use it to retrieve the current value
  // of the TextField.
  TextEditingController textFieldController = TextEditingController();

  @override
  void dispose() {
    // Clean up the controller when the widget is disposed.
    textFieldController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    // Fill this out in the next step.
    return null;
  }

  static Color _normal = Colors.blue;
  static Color _destructive = Colors.red;  
  bool phoneIsInDarkMode = false;

  /// show the OS Native dialog
   showOSDialog(BuildContext context, String message,
      String firstButtonText, Function firstCallBack, bool isSuccess) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        phoneIsInDarkMode = _phoneIsInDarkMode(context);        
        return _dialog(
          context, message, firstButtonText, firstCallBack, isSuccess,
          firstActionStyle: ActionStyle.normal,                    
        );        
      },
    );
  }

  _ensureCallbackExecutedProperly(Function callback, bool hasTextbox){
    if(callback != null){
      if(hasTextbox){
        callback(textFieldController.text);
      }else{
        callback();
      }
    }
  }

  _ensureAndroidTextbox(bool thereIsTextbox, String message){
     var result;
     if(thereIsTextbox){
       result = TextField(
           controller: textFieldController,
           decoration: InputDecoration(
             border: OutlineInputBorder(),
             labelText: message,
           ),
           style: TextStyle(
             fontFamily: 'Roboto-Regular',
           )
       );
     }else{
       result = Text(
         message,
           style: TextStyle(
             fontFamily: 'Roboto-Regular',
             )
       );
     }
     return result;
  }


  _phoneIsInDarkMode(BuildContext context){
    var isDark = false;
    final Brightness brightnessValue = MediaQuery.of(context).platformBrightness;
    isDark = brightnessValue == Brightness.dark;
    return isDark;
  } 

  _ensureSuccessOrFailIcon(bool isSuccess){
    var result;
    if(isSuccess){
        result = Icon(
          Icons.check_circle,
          color: Colors.green,
          size: 100.0,          
        );
    }else{
      result = Icon(
          Icons.error,
          color: Colors.red,
          size: 100.0,          
        );
    }
    return result;
  }

  Widget _dialog(BuildContext context, String message,
      String firstButtonText, Function firstCallback,
      bool isSuccess,
      {ActionStyle firstActionStyle = ActionStyle.normal}) {
    List<CupertinoDialogAction> actions = [];

    var messageTextbox = Padding(
      padding: EdgeInsets.fromLTRB(0, 0, 0, 15),
      child: Text(
        message,
        style: TextStyle(
            fontFamily: 'Helvetica',   
            fontSize: 25     
        )
      )
    );

    var successFailSymbol = _ensureSuccessOrFailIcon(isSuccess);

    actions.add(
      CupertinoDialogAction(
        isDefaultAction: true,
        onPressed: () {
          Navigator.of(context).pop();
          if(firstCallback != null){
            firstCallback();
          }
        },
        child: Text(firstButtonText,
          style: TextStyle(
              color: (firstActionStyle == ActionStyle.important_destructive ||
                  firstActionStyle == ActionStyle.destructive)
                  ? _destructive
                  : _normal,
              fontFamily: 'Helvetica',              
              fontWeight: (firstActionStyle == ActionStyle.important_destructive ||
                  firstActionStyle == ActionStyle.important)
                  ? FontWeight.bold
                  : FontWeight.normal
              ),
        ),
      ),
    );    

    return CupertinoAlertDialog(
      title: successFailSymbol,
      content: messageTextbox,
      actions: actions,
    );
  }
}