import 'dart:async';
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:web_socket_channel/io.dart';
import 'package:web_socket_channel/web_socket_channel.dart';
import 'package:barcode_scan/barcode_scan.dart';
import 'data_models/BarcodeData.dart';
import 'CustomPopup.dart';
import 'package:http/http.dart' as http;



void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        // appBar: AppBar(
        //   title: Text('Web Socket Demo'),
        // ),
        body: WebSocketDemo(),
      ),
    );
  }
}

class WebSocketDemo extends StatefulWidget {
  final WebSocketChannel channel =
      // IOWebSocketChannel.connect('ws://echo.websocket.org');
      // IOWebSocketChannel.connect('ws://localhost:8765');
      IOWebSocketChannel.connect('ws://192.168.0.147:8765'); //Raspberry Pi
      

  @override
  _WebSocketDemoState createState() => _WebSocketDemoState(channel: channel);
}

class _WebSocketDemoState extends State<WebSocketDemo> {
  String BASE_URL_API =  "http://192.168.0.146:8080/api/users/";
  String OBJ_IS_METAL = "obj_is_metal"; //to match with the raspberry pi code's response
  String OBJ_IS_NOT_METAL = "obj_not_metal";
  String cardNumber = "";
  final WebSocketChannel channel;
  final inputController = TextEditingController();
  final cardNumberInputController = TextEditingController();
  List<String> message_list = [];
  BarcodeData barcodeData;
  String isMetalResult;
  bool obj_is_metal;

  String debugText = "";
  

  @override initState(){
    barcodeData = new BarcodeData();
    setState(() {
      barcodeData.barcodeData = "";    
      debugText = "";  
    });

    super.initState();
  }

  _WebSocketDemoState({this.channel}) {
    channel.stream.listen((data) {
      setState(() {
        print(data);
        message_list.add(data);
        isMetalResult = data;
      });

      print("CALLING _received_metal_response_from_rpi()");
      _received_metal_response_from_rpi(isMetalResult);
    });    
  }

Future<BarcodeData> _scanBarcode() async {
    barcodeData = new BarcodeData();
    try {
      String scanResult = await BarcodeScanner.scan();
      setState(() {
        barcodeData.barcodeData = scanResult;        
      });
    } on PlatformException catch (ex) {
      if (ex.code == BarcodeScanner.CameraAccessDenied) {
        setState(() {
          barcodeData.barcodeData = "Camera permission was denied";
        });
      } else {
        setState(() {
          barcodeData.barcodeData = "Unknown Error $ex";
        });
      }
    } on FormatException {
      setState(() {
        barcodeData.barcodeData = "123456789"; //You pressed the back button before scanning anything        
      });
    } catch (ex) {
      setState(() {
        barcodeData.barcodeData = "Unknown Error $ex";
      });
    }
    return barcodeData;
  }

  _websocket_test(BuildContext context){
    //send message from textbox
    if (inputController.text.isNotEmpty) {
      print(inputController.text);
      channel.sink.add(inputController.text);
    }
    inputController.text = '';
  }

  _test_api(BuildContext context){
    cardNumber = "123";
    Future futureResponse = submitBarcodeToServer("0987", cardNumber);
      futureResponse.then((futureResponse){
        print(futureResponse.body);
        setState(() {
          debugText = futureResponse.body['message'];
        });
        _received_metal_response_from_rpi(OBJ_IS_METAL); //DEBUG
      });
  }

  _test_textbox(BuildContext context){
    setState(() {
      debugText = "boogaloo!!";
    });
  }

  _test_popup(BuildContext foo){    
    _display_popup(context, "Soda Can cannot be redeemed twice", false);    
  }

  _scanButtonPressed(BuildContext context){  
    cardNumber = cardNumberInputController.text;     
    //barcode scan
    //User has to scan a QR code, which is then stored into futureScannedCode
    Future<BarcodeData> futureScannedCode = _scanBarcode();    
    //After a Barcode has been scanned, it is stored into class variable "myData"    
    futureScannedCode.then((futureScannedCode) {
      barcodeData = futureScannedCode;

      //Tell RaspberryPi to determine if object is metal
      // _tell_raspberry_pi_to_determine_if_obj_is_metal();
      // _received_metal_response_from_rpi(OBJ_IS_METAL); //DEBUG
      _send_message_to_raspberry_pi("detect_metal");
      cardNumberInputController.text = "";
    });
  }

  _display_popup(BuildContext context, String message, bool isSuccess){
    CustomPopupState myPopup = new CustomPopupState(); 
    myPopup.showOSDialog(context, message, "OK", null, isSuccess);
  }

  _received_metal_response_from_rpi(String metalData){
    print("INSDE _received_metal_response_from_rpi");
    if(metalData == OBJ_IS_METAL){
      setState(() {
        this.obj_is_metal = true;
        debugText = "OBJECT IS METAL!";
        print("object IS metal");
      });
      Future futureResponse = submitBarcodeToServer(this.barcodeData.barcodeData, this.cardNumber);
      futureResponse.then((futureResponse){
        _process_server_response(futureResponse);
      });      
    }else if(metalData == OBJ_IS_NOT_METAL){          
      _display_popup(context, "Object is NOT metal", false);
      setState(() {
        debugText = "OBJECT IS NOT METAL!";  
        print(debugText);
        this.obj_is_metal = false;
        message_list.add("Object is not metal");    
      });          
    }
  }

  _process_server_response(http.Response response){
    Map<String, dynamic> jsonResponse = jsonDecode(response.body);
    if(response.statusCode == 200 && this.obj_is_metal){
      _send_message_to_raspberry_pi("run_servo");
      _display_popup(context, jsonResponse["message"], true);
      setState(() {
        debugText = jsonResponse['message'];
        print("200 res: " + debugText);  
      });
    }else{      
      _display_popup(context, jsonResponse['message'], false);
      setState(() {
        debugText = jsonResponse['message'];
        print("NOT 200 res: " + debugText);   
      });
      
    }
    message_list.add(debugText); 
  }  

  _send_message_to_raspberry_pi(String msg){
    print("sending " + msg + " to RPi");
    channel.sink.add(msg);
  }

  // Future<http.Response> submitBarcodeToServer(String barcode) {
  //   var url = this.BASE_URL_API + this.cardNumber;    
  //   print("submitting barcode: " + barcode);
  //   print("calling http.put(): " + url);       
  //   return http.post(
  //     url,
  //     headers: {'Content-Type': 'application/x-www-form-urlencoded'},
  //     body: jsonEncode(<String, String>{
  //       'barcode': barcode,        
  //     }),
  //   );
  // }
  
  Future submitBarcodeToServer(String barcode, String cardNumber) async{    
    var url = this.BASE_URL_API + cardNumber;    
    var response = await http.post(
      url,
      headers: <String, String>{
        'Content-Type': 'application/x-www-form-urlencoded',
      },
      body: {
        'barcode': barcode
      }
    );

    if(response.statusCode != 200){      
      // clientListJSON = jsonDecode(response.body)['nearby_clients'] as List;      
      print("ERROR: " + response.body); 
      _display_popup(context, response.body, false);     
    }
    return response;

  }


  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: <Widget>[
          Padding(
            padding: EdgeInsets.all(10.0),
            child: Row(
              children: <Widget>[
                Expanded(
                  child: TextField(
                    controller: inputController,
                    decoration: InputDecoration(
                      labelText: 'Send Message',
                      border: OutlineInputBorder(),
                    ),
                    style: TextStyle(fontSize: 22),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: RaisedButton(
                    child: Text(
                      'Send',
                      style: TextStyle(fontSize: 20),
                    ),
                    onPressed: () => _websocket_test(context),
                  ),
                ),                
              ],
            ),
          ),
          Row(
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: RaisedButton(
                  child: Text(
                    'Test Popip',
                    style: TextStyle(fontSize: 20),
                  ),
                  onPressed: () => _test_popup(context),
                ),
              ),
              Text(barcodeData.barcodeData),
            ],
          ),
          Column(
            children: <Widget>[
              Center(
                child: TextField(
                  controller: cardNumberInputController,
                  decoration: InputDecoration(
                    labelText: 'Enter Card Number',
                    border: OutlineInputBorder(),
                  ),
                  style: TextStyle(fontSize: 22),
                ),
              ),
              Center(
                child: RaisedButton(
                  child: Text(
                    'Scan Barcode',
                    style: TextStyle(fontSize: 20),
                  ),
                  onPressed: () => _scanButtonPressed(context),
                ),
              ),
              Center(
                child: Text('${this.debugText}'),
              ),
            ],
          ),
          Expanded(
            child: show_message_list(),
          ),
        ],
      ),
    );
  }

  ListView show_message_list() {
    List<Widget> listWidget = [];

    for (String message in message_list) {
      listWidget.add(ListTile(
        title: Container(
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Text(
              message,
              style: TextStyle(fontSize: 22),
            ),
          ),
          color: Colors.teal[50],
          height: 60,
        ),
      ));
      message_list = []; // clear out the message list
    }

    return ListView(children: listWidget);
  }

  @override
  void dispose() {
    inputController.dispose();
    widget.channel.sink.close();
    super.dispose();
  }
}
