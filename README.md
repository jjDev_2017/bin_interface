# bin_interface

A new Flutter project.

## Useful commands
    - flutter build ios
    - flutter packages get
    - flutter doctor    


## Troubleshooting iOS build
    - rm -rf ios/
    - flutter create .
    - sudo gem install cocoapods --pre     (to update cocoapods)
    - pod install
    - flutter clean
    - flutter build ios    

## Prerequisites for running the project
    - MAKE SURE TO OPEN Runner.xcworkspace
    - Open ios/Runner/Info.plist
        - Under "Information Property List", find and click "+" circle button to add an entry
        - Look for "Privacy - Camera Usage Description" in the drop down and enter some string for its Value column

## Running the project
    - download all dependencies: flutter pub get
    - activate the emulator: open -a simulator
    - run your project: flutter run
